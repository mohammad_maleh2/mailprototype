# After checkout please follow the following steps: #

* open terminal at the project location.
* run npm install.
* run gulp build.
* public folder is gonna be created (including all the js and css files bundled and sweet :) ).
* set public folder as a  resource root (to not miss icons and images).
* the index is gonna be located at /public/index.html .
* run index.html and hopefully you gonna like it.