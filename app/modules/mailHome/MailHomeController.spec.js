/*jshint expr: true*/

'use strict';

describe('MailHomeController', function () {
    console.log('herer')

    var vm,$rootScope,$scope,$q,$controller;
    beforeEach(angular.mock.module('GKFX_PartnerPortal'));
    beforeEach(function () {
        angular.mock.inject(function (_$controller_, _$rootScope_,_$q_,_CoreService_,_ApiService_) {

            $rootScope = _$rootScope_;
            $q = _$q_;
            //console.log(_$controller_)
            $controller = _$controller_;
            $scope = $rootScope.$new();
            $rootScope.userData = $rootScope.content = {};
            $rootScope.content.showDisclaimer = function(){};
            vm = $controller('RemoveSubContractCtrl as RemoveSubContractVm', {
                $scope: $scope,
                vm: this
            });

        });


    });
    it('should vm exist', function () {
        expect(vm).to.not.be.undefined;



    });
    it('functions check', function () {

        expect(vm.selectMasterContract).to.not.be.undefined;
        expect(vm.selectSubContract).to.not.be.undefined;
        expect(vm.removeSubContract).to.not.be.undefined;
        expect(vm.getMasterContracts).to.not.be.undefined;
        expect(vm.getSubContracts).to.not.be.undefined;
        expect(vm.getSubPartner).to.not.be.undefined;
        expect(vm.getMasterPartner).to.not.be.undefined;


    });

});