'use strict';

function mailHomeRoutes($stateProvider) {
    // setting the route of my module
    var mailHome = {
        name: 'mailHome', // state name
        url: '/mail-home', // url path that activates this state
        template: '<div mail-home-view></div>', // generate the Directive "homeView" - when calling the directive in HTML, the name must not be camelCased
        data: {
            moduleClasses: 'page', // assign a module class to the <body> tag
            pageClasses: 'mailHome', // assign a page-specific class to the <body> tag
            pageTitle: 'Home', // set the title in the <head> section of the index.html file
            //pageDescription: 'Meta Description goes here' // meta description in <head>
        }
    };

    $stateProvider.state(mailHome);

}

mailHomeRoutes.$inject = ['$stateProvider'];
module.exports = mailHomeRoutes;