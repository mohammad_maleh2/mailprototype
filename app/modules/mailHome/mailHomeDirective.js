'use strict';
var fs = require('fs');
// defining the view
var mailHomeDirectiveHtml = fs.readFileSync(__dirname + '/mailHome.html', 'utf8');

module.exports = function mailHomeDirective() {
    return {
        // defining the controller
        controller: 'MailHomeCtrl',
        // defining the main variable of the controller and how the view gonna see it
        controllerAs: 'mailHomeVm',
        bindToController: true,
        restrict: 'EA',
        scope: true,
        template: mailHomeDirectiveHtml
    };
};