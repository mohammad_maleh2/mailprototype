'use strict';
// Controller naming conventions should start with an uppercase letter
function MailHomeCtrl() {

    var vm = this ;  // this item its gonna by my only  item used for the view ... i put every function and variable needed in the view here ...
    // it makes it easier to test and to load in another modules
    // this will presented on the view as "mailHomeVm"

    // to handle clicking on an item
    vm.clickOnNavigationItem = clickOnNavigationItem
    // to remove a singe item
    vm.removeItem = removeItem
    // to remove multiple items when checked
    vm.deleteMultipleItems = deleteMultipleItems
    // to mark all items checked or unchecked
    vm.checkAllFunction = checkAllFunction
    // to remove all the checks when a filter is entered ... its gonna generate bugs if unfiltered item is checked :)
    vm.filterEntered = filterEntered
    // to handle mark as read for checked items functionality
    vm.markAsRead = markAsRead
    // to handle mark as unread for checked items functionality
    vm.markAsUnread = markAsUnread
    // to handle the set of buttons (delete, read, unread) on the top
    vm.checkControl = checkControl

    // loading the json file
    vm.mailbox = {
        "messages": [
            {
                "uid": "21",
                "sender": "Ernest Hemingway",
                "subject": "animals",
                "message": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
                "time_sent": 1459239867
            },
            {
                "uid": "22",
                "sender": "Stephen King",
                "subject": "adoration",
                "message": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
                "time_sent": 1459248747
            },
            {
                "uid": "23",
                "sender": "Virgina Woolf",
                "subject": "debt",
                "message": "The story is about an obedient midwife and a graceful scuba diver who is in debt to a fence. It takes place in a magical part of our universe. The story ends with a funeral.",
                "time_sent": 1456767867
            },
            {
                "uid": "24",
                "sender": "George Orwell",
                "subject": "chemist",
                "message": "This is a tale about degeneracy. The story is about a chemist. It takes place in a manufacturing city. The story begins with growth.",
                "time_sent": 1456744407
            },
            {
                "uid": "25",
                "sender": "James Joyce",
                "subject": "nuclear engineer",
                "message": "The story is about an ugly nuclear engineer. It starts in a manufacturing city in Africa. The future of warfare is a major part of this story.",
                "time_sent": 1456733427
            },
            {
                "uid": "26",
                "sender": "Jane Austen",
                "subject": "treasure-hunter",
                "message": "The story is about a treasure-hunter and a treasure-hunter who is constantly annoyed by a misguided duke. It takes place on a forest planet in a galaxy-spanning commonwealth. The critical element of the story is a door being opened",
                "time_sent": 1456730427
            }
        ]
    }
    // this variable will present the message content  on the view
    vm.messageContent = {};

    vm.showMessageContent = false;

    // formating the json file adding new items like check and formatted date
    angular.forEach(vm.mailbox.messages,function(message){
        // i dont like to format the actual date that has been sent from backend ... had a lot of bugs in the past because of this
        // i just add another field in json file to show on the view
        message.timeSentToShow = moment(message.time_sent).format('dddd DD MMMM, hh:mm');
        // adding read property to json to check if the user has read it or not
        message.read = false;
        // adding email just for the view and style sake
        message.email = message.sender.replace(/ /g,".")+"@gmail.com";


    });
    function checkControl (){
        // checking if at least one item is checked to show the set of buttons at the top
        vm.atLeastOneItemChecked = false ;
        angular.forEach(vm.mailbox.messages, function (message) {
            if (message.checked ) {
                vm.atLeastOneItemChecked = true ;

            }
        })
    };

    function clickOnNavigationItem  (message){
        message.read = true;
        vm.messageContent = message ;
        vm.showMessageContent = true;
        checkControl();
    };

    function filterEntered  (){
        // removing all the checks from items because its gonna generate bugs i belive
       angular.forEach(vm.mailbox.messages, function (message) {
           message.checked = false;
       })
       vm.checkAll = false;
        checkControl();
    };

    function checkAllFunction  (){
        // check all items or uncheck
        angular.forEach(vm.filteredItems, function (message) {

            if (vm.checkAll)
                message.checked = true;
            else
                message.checked = false;

        })
        checkControl();

    };
    function removeItem  (toRemoveMessage){

        angular.forEach(vm.mailbox.messages, function (message,index) {
            if (message.uid == toRemoveMessage.uid){
                vm.mailbox.messages.splice(index, 1);
                // checking if the user is curently viewing the item ... if he is viewing i will just make the view empty
                if (vm.messageContent.uid == message.uid){

                    vm.messageContent = {};
                    vm.showMessageContent = false;
                }
            }

        });

    };
    function deleteMultipleItems  (){
        // had to for loop backwards  because of deleting item of array and index conflict
        for (var i = vm.mailbox.messages.length -1; i >= 0; i--){

            if(vm.mailbox.messages[i].checked){
                if (vm.messageContent &&(vm.messageContent.uid == vm.mailbox.messages[i].uid) ){

                    vm.messageContent = {};
                    vm.showMessageContent = false;
                }
                vm.mailbox.messages.splice(i, 1);

            }

        }
        vm.checkAll = false;
        checkControl();


    }
    function markAsRead  (){
        angular.forEach(vm.mailbox.messages, function (message) {
            if (message.checked){
                message.checked = false;
                message.read = true;

            }
        });
        vm.checkAll = false;
        checkControl();
    };
    function markAsUnread  (){
        angular.forEach(vm.mailbox.messages, function (message) {
            if (message.checked){
                message.read = false;
                message.checked = false;

            };
        });
        vm.checkAll = false;
        checkControl();
    };

}


MailHomeCtrl.$inject = [];
module.exports = MailHomeCtrl;