/**
 * Created by mohammad.maleh on 9/24/2016.
 */
'use strict';
module.exports = angular.module('modules',
    [
        //loading the module
        require('./mailHome').name

    ])
    .controller('MainCtrl', require('./MainController'));