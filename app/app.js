/**
 * Created by mohammad.maleh on 9/24/2016.
 */
// here is the main modules
var angular = require('angular');

module.exports = angular.module('Mail_Prototype',

    [
        // calling the common module where i put directives filters constants ect ...
        require('./common/common.js').name,
        // calling the main module where i call al the pages modules from
        require('./modules').name
    ])


